<?php

namespace App\Classes;

use Illuminate\Http\Response;

class HtmlResponder
{
    public function format($responseArray)
    {
        $response = new Response();

        $data = '<html><head></head><body>';
        $data .= "<p>Total images: {$responseArray['total_images']}</p>";

        foreach ($responseArray['results'] as $imageData) {
            $data .= "<hr>Image width: {$imageData['width']}";
            $data .= "<br>Image height: {$imageData['height']}<br>";
            $data .= "<img src=\"{$imageData['url']}\">";
        }

        $data .= '</body></html>';

        $response->setContent($data);
        $response->setStatusCode(200);

        return $response;
    }
}