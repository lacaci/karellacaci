<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('ip', 20)->unique()->nullable();
            $table->string('api_token', 128)->unique();
            $table->rememberToken();
            $table->timestamps();
        });

        // Create the test user
        DB::table('users')->insert([
            'name' => 'test_user',
            'email' => 'test_user@paperstreetmedia.com',
            'ip' => '0.0.0.0',
            'api_token' => md5('test_token'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
