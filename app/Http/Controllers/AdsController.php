<?php

namespace App\Http\Controllers;

use App\Classes\HtmlResponder;
use App\Classes\TagHandler;
use App\Exceptions\InvalidRequestParamException;
use App\Models\Image;
use App\Models\Tag;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AdsController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        // Assign authentication middleware to the required methods
        $this->middleware('auth', [
            'only' => [
                'index',
            ]
        ]);
    }


    public function index(Request $request)
    {
        $response = [];
        $results = [];
        $tagHandler = new TagHandler(new Tag());

        $requiredParameters = [
            'number_of_images' => 'integer',
            'ad_keywords' => 'string',
        ];

        try {
            $this->validate->request($request, $requiredParameters);

            $keywords = explode(',', $request->input('ad_keywords'));
            $maxResults = $request->input('number_of_images');

            $tags = $tagHandler->getCollection($keywords);

            $counter = 0;

            /** @var Tag $imageTag */
            foreach ($tags as $imageTag) {

                $remaining = $maxResults - $counter;

                if ($remaining == 0) {
                    break;
                }

                /**
                 * Fetch the associated images for this tag but do not exceed the requested number of results
                 * This method is very crude but will get the job done for this test. If there are not
                 * enough results for this tag it will continue to the next
                 */
                $images = $imageTag->images()->limit($remaining)->get();

                /** @var Image $image */
                foreach ($images as $image) {

                    // Prevent duplicate results
                    if (!in_array($image->url, $results)) {
                        $results[$counter]['url'] = $image->url;
                        $results[$counter]['width'] = $image->width;
                        $results[$counter]['height'] = $image->height;

                        $counter++;
                    }
                }
            }

            // Construct the image results array
            $response['total_images'] = count($results);
            $response['results'] = $results;

        } catch (InvalidRequestParamException | ModelNotFoundException $e) {
            return $this->respondInternalError($e->getMessage());
        }

        // Only respond in html format if specifically requested (only included for the sake of testing)
        if ($request->has('format') and $request->input('format') == 'html') {
            $html = new HtmlResponder();

            return $html->format($response);
        }

        return $this->respondSuccess($response);
    }
}
