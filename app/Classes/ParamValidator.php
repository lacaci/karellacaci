<?php

namespace App\Classes;

use App\Exceptions\InvalidRequestParamException;
use Illuminate\Http\Request;

class ParamValidator
{
    // Define supported data types
    private const INTEGER = 'integer';
    private const ARRAY = 'array';
    private const STRING = 'string';

    /**
     * Perform a very crude parameter type validation
     *
     * @param Request $request
     * @param array $requiredParameters
     * @throws InvalidRequestParamException
     */
    public function request(Request $request, array $requiredParameters)
    {
        foreach ($requiredParameters as $name => $requiredType) {
            // make sure the parameter was passed in the request and is of valid type
            if (!$request->has($name) or !$this->validType($request->input($name), $requiredType)) {
                throw new InvalidRequestParamException("Parameter '{$name}' is required and must be a valid {$requiredType}.");
            }
        }
    }

    /**
     * Determine if a parameter is of the required type as long as its supported
     *
     * @param $param
     * @param $requiredType
     * @return bool
     */
    private function validType($param, $requiredType)
    {
        switch ($requiredType) {
            case self::INTEGER:
                // Force integers to be greater than zero
                return (is_numeric($param) && (int)$param > 0);
            case self::ARRAY:
                return (is_array($param));
            case self::STRING:
                // Force strings to be at least 3 characters long
                return (is_string($param) && strlen($param) >= 3);
            default:
                return false;
        }
    }
}