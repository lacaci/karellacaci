<?php

use App\Models\Image;
use App\Models\ImageType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateSampleData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Insert some basic tags for the sake of the example
        DB::table('tags')->insert(['name' => 'bird']);
        DB::table('tags')->insert(['name' => 'fruit']);
        DB::table('tags')->insert(['name' => 'falcon']);
        DB::table('tags')->insert(['name' => 'predator']);
        DB::table('tags')->insert(['name' => 'shark']);
        DB::table('tags')->insert(['name' => 'hammerhead']);
        DB::table('tags')->insert(['name' => 'wolf']);
        DB::table('tags')->insert(['name' => 'guava']);
        DB::table('tags')->insert(['name' => 'banana']);
        DB::table('tags')->insert(['name' => 'parrot']);
        DB::table('tags')->insert(['name' => 'apple']);
        DB::table('tags')->insert(['name' => 'tiger']);

        $this->insertImage('https://imageshack.com/a/img923/4473/atKXra.jpg', 'jpg', ['bird', 'parrot'], 960, 641);
        $this->insertImage('https://imageshack.com/a/img923/831/bRtK3V.jpg', 'jpg', ['bird'], 600, 900);
        $this->insertImage('https://imageshack.com/a/img922/5343/lMwhmW.jpg', 'jpg', ['bird', 'falcon', 'predator'], 800, 600);
        $this->insertImage('https://imageshack.com/a/img924/5519/G1NqT7.jpg', 'jpg', ['apple, fruit,'], 1100, 619);
        $this->insertImage('https://imageshack.com/a/img924/1616/s0Adsa.jpg', 'jpg', ['shark', 'predator'], 600, 646);
        $this->insertImage('https://imageshack.com/a/img923/2868/4WoiLn.jpg', 'jpg', ['fruit', 'banana'], 203, 249);
        $this->insertImage('https://imageshack.com/a/img923/4609/JfRWRj.jpg', 'jpg', ['fruit', 'guava'], 493, 335);
        $this->insertImage('https://imageshack.com/a/img922/1560/Uj8J6E.jpg', 'jpg', ['wolf', 'predator'], 324, 205);
        $this->insertImage('https://imageshack.com/a/img924/3052/BryCF6.jpg', 'jpg', ['tiger', 'predator'], 800, 500);
        $this->insertImage('https://imageshack.com/a/img922/3020/EwGrwu.jpg', 'jpg', ['shark', 'predator', 'hammerhead'], 500, 375);
        $this->insertImage('https://imageshack.com/a/img924/5720/jQKpiu.jpg', 'jpg', ['bird'], 800, 450);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    /**
     * Insert image and attach the corresponding tags.
     *
     * @param $url
     * @param $type
     * @param $tags
     * @param $width
     * @param $height
     */
    private function insertImage($url, $type, $tags, $width, $height)
    {
        $tagIdArray = [];
        $image = new Image();

        $image->url = $url;
        $image->width = $width;
        $image->height = $height;

        // Associate the image type
        $type = $image->imageType()->getModel()->where('name', '=', $type)->first();
        $image->imageType()->associate($type);

        $image->save();

        // Get the tags collection from the image model relationship
        $tags = $image->tags()->getModel()->whereIn('name', $tags)->get();

        // Transfer the tag id to array for association
        foreach ($tags as $tag) {
            $tagIdArray[] = $tag->id;
        }

        $image->tags()->attach($tagIdArray);
    }
}
