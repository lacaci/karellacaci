<?php

namespace App\Classes;

use App\Models\Tag;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TagHandler
{
    /** @var Tag */
    protected $tag;

    /**
     * TagHandler constructor.
     * @param Tag $tag
     */
    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }


    /**
     * Get the tag objects using the parameter string or throw an exception if none found
     *
     * @param array $keywords
     * @return mixed
     */
    public function getCollection(array $keywords)
    {
        // Match the tags that relate to the requested parameter
        $results = $this->tag->whereIn('name', $keywords)->get();

        if ($results->isEmpty()) {
            throw new ModelNotFoundException('Tags not found');
        }

        return $results;
    }
}