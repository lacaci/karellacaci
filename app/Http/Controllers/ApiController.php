<?php

namespace App\Http\Controllers;


use App\Classes\ParamValidator;
use App\Traits\ApiResponder;

/**
 * Serves as an extra layer before the controller parent class in where to spply global behaviour like
 * rate-limiting and normalize responses through the ApiResponder trait.
 * This class is abstract on purpose as it provides no functionality on its own.
 *
 * Class ApiController
 * @package App\Http\Controllers
 */
abstract class ApiController extends Controller
{
    use ApiResponder;

    protected $validate;

    /**
     * ApiController constructor.
     */
    public function __construct()
    {
        // Use the rate limite number defined in the .env file or default to 1 / sec
        $rateLimit = env('RATE_LIMIT', 20);

        /**
         * Apply the request throttle rate-limit middleware to all methods
         */
        $this->middleware("throttle:{$rateLimit}");

        // Instantiate the request parameter validator for use in child classes
        $this->validate = new ParamValidator();
    }


}