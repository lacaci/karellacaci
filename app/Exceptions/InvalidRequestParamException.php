<?php

namespace App\Exceptions;

use Exception;

/**
 * Custom exception used when a request contains invalid parameters
 *
 * Class InvalidItemTypeException
 * @package App\Exceptions
 */
class InvalidRequestParamException extends Exception
{

}