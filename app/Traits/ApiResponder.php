<?php

namespace App\Traits;

use Illuminate\Http\Response;

/**
 * Normalizes all responses and provides methods for common response types i.e. Internal Errors, Success, etc.
 *
 * Trait ApiResponder
 * @package App\Traits
 */
trait ApiResponder
{
    protected $statusCode = 200;

    /**
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param $data
     * @param int $statusCode
     * @return Response
     * @internal param string $message
     */
    public function respondSuccess($data = [], $statusCode = 200)
    {
        return $this->setStatusCode($statusCode)->respond([
            'data' => $data
        ]);
    }


    /**
     * @param string $message
     * @param int $statusCode
     * @return Response
     */
    public function respondNotFound($message = 'Not Found', $statusCode = 404)
    {
        return $this->setStatusCode($statusCode)->respondWithError($message);
    }

    /**
     * @param string $message
     * @param int $statusCode
     * @return Response
     */
    public function respondUnauthorized($message = 'Unauthorized', $statusCode = 401)
    {
        return $this->setStatusCode($statusCode)->respondWithError($message);
    }

    /**
     * @param string $message
     * @param int $statusCode
     * @return Response
     */
    public function respondRateLimit($message = 'Too Many Attempts.', $statusCode = 429)
    {
        return $this->setStatusCode($statusCode)->respondWithError($message);
    }

    /**
     * @param string $message
     * @param int $statusCode
     * @return Response
     */
    public function respondInternalError($message = 'Internal Error', $statusCode = 500)
    {
        return $this->setStatusCode($statusCode)->respondWithError($message);
    }


    /**
     * @param $message
     * @return Response
     */
    private function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'code' => $this->getStatusCode(),
            ]
        ]);
    }


    /**
     * @param $data
     * @param array $headers
     * @return Response
     */
    private function respond($data, $headers = [])
    {
        $response = new Response();

        $response->setContent($data);
        $response->setStatusCode($this->getStatusCode());

        return $response;
    }


}