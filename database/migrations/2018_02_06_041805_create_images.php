<?php

use App\Models\ImageType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the table that holds the supported image types
        Schema::create('image_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 60);
            $table->timestamps();
        });

        // Define the initial supported image types
        $imageType = new ImageType();

        $imageType->insert(['name' => 'jpg']);
        $imageType->insert(['name' => 'gif']);
        $imageType->insert(['name' => 'png']);

        // Create the table that holds the image file names in hash form
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('image_type_id');
            $table->string('url', 255);
            $table->unsignedSmallInteger('width');
            $table->unsignedSmallInteger('height');
            $table->timestamps();

            $table->foreign('image_type_id')->references('id')->on('image_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
        Schema::dropIfExists('image_types');
    }
}
