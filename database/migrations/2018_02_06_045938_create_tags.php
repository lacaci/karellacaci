<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create the table that holds the tags for the images
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->timestamps();
        });

        // Create the relational table
        Schema::create('image_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('image_id');

            $table->foreign('tag_id')->references('id')->on('tags');
            $table->foreign('image_id')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_tag');
        Schema::dropIfExists('tags');
    }
}
